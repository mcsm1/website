package main

import (
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/davidjmacdonald1/mcsm/website/cmd"
	"gitlab.com/davidjmacdonald1/mcsm/website/db"
	"gitlab.com/davidjmacdonald1/mcsm/website/handler"
)

func main() {
	godotenv.Load()
	cmd.HostConn = cmd.MustMakeConnection(true)
	cmd.WOLConn = cmd.MustMakeConnection(false)

	db.MustConnect()
	router := handler.MakeRouter()

	port := ":" + os.Getenv("PORT")
	log.Println("Starting server on port " + os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(port, router))
}
