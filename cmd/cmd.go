package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"gitlab.com/davidjmacdonald1/mcsm/api/out"
)

var ZeroState = MakeState(out.Output{})

type State out.Output

func MakeState(o out.Output) State {
	if o.Server == nil {
		o.Server = &out.Server{}
	}
	return State(o)
}

func MakeErrorState(err string) State {
	return MakeState(out.Output{Error: err})
}

var HostConn connection
var WOLConn connection

func SendHost(command string) State {
	if !PingHost() {
		return ZeroState
	}

	isShutdown := command == "shutdown"
	path, app := os.Getenv("HOST_CLI_PATH"), os.Getenv("HOST_CLI_APP")
	command = fmt.Sprintf("cd %s && %s %s", path, app, command)

	stdout, err := HostConn.Send(command)
	if err != nil {
		if isShutdown {
			return ZeroState
		}
		log.Println("Error sending host command:", err)
		return MakeErrorState("internal server error: sending command")
	}

	var output out.Output
	err = json.Unmarshal([]byte(stdout), &output)
	if err != nil {
		log.Println("Error parsing json from host:", err)
		return MakeErrorState("internal server error: parsing result")
	}

	if output.Error != "" {
		log.Println("CLI Error:", output.Error)
		output.Error = "internal server error: host error"
	}
	return MakeState(output)
}

func WakeHost() bool {
	if PingHost() {
		return true
	}

	_, err := WOLConn.Send(os.Getenv("WOL_COMMAND"))
	if err != nil {
		log.Println("WOL command failed:", err)
		return false
	}

	totalSecs, waitSecs := 60, 5
	for range totalSecs / waitSecs {
		time.Sleep(time.Duration(waitSecs) * time.Second)
		if PingHost() {
			return true
		}
	}
	log.Println("WOL took too long")
	return false
}

func PingHost() bool {
	ip, port := os.Getenv("HOST_IP"), os.Getenv("HOST_SSH_PORT")
	addr := fmt.Sprintf("%s:%s", ip, port)
	conn, err := net.DialTimeout("tcp", addr, 200*time.Millisecond)
	if err != nil {
		return false
	}
	conn.Close()
	return true
}
