package cmd

import (
	"fmt"
	"log"
	"os"
	"sync"

	"golang.org/x/crypto/ssh"
)

type connection struct {
	client *ssh.Client
	mu     sync.Mutex
	addr   string
	config ssh.ClientConfig
}

func MustMakeConnection(toHost bool) connection {
	var port, user, hostKeyStr string
	if toHost {
		port, user = os.Getenv("HOST_SSH_PORT"), os.Getenv("HOST_SSH_USER")
		hostKeyStr = os.Getenv("HOST_SSH_KEY")
	} else {
		port, user = os.Getenv("WOL_SSH_PORT"), os.Getenv("WOL_SSH_USER")
		hostKeyStr = os.Getenv("WOL_SSH_KEY")
	}

	key, pass := []byte(os.Getenv("SSH_KEY")), []byte(os.Getenv("SSH_PASS"))
	signer, err := ssh.ParsePrivateKeyWithPassphrase(key, pass)
	if err != nil {
		forHost := fmt.Sprintf("(for host? %t): ", toHost)
		log.Fatal("Unable to parse private key ", forHost, err)
	}

	hostKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(hostKeyStr))
	if err != nil {
		forHost := fmt.Sprintf("(for host? %t): ", toHost)
		log.Fatal("Unable to parse public key ", forHost, err)
	}

	addr := fmt.Sprintf("%s:%s", os.Getenv("HOST_IP"), port)
	config := ssh.ClientConfig{
		User:              user,
		HostKeyCallback:   ssh.FixedHostKey(hostKey),
		HostKeyAlgorithms: []string{ssh.KeyAlgoED25519},
		Auth: []ssh.AuthMethod{
			ssh.Password(string(pass)),
			ssh.PublicKeys(signer),
		},
	}
	return connection{addr: addr, config: config}
}

func (c *connection) Send(command string) (string, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	sess, err := c.newSession(2)
	if err != nil {
		return "", err
	}
	defer sess.Close()

	output, err := sess.CombinedOutput(command)
	if err != nil {
		return "", err
	}
	return string(output), nil
}

func (c *connection) newSession(attempts int) (*ssh.Session, error) {
	if c.client == nil {
		client, err := ssh.Dial("tcp", c.addr, &c.config)
		if err != nil {
			log.Println("Error creating ssh connection:", err)
			return nil, err
		}
		c.client = client
	}

	sess, err := c.client.NewSession()
	if err != nil {
		c.client = nil
		if attempts > 1 {
			return c.newSession(attempts - 1)
		}
		log.Println("Error creating ssh session:", err)
	}
	return sess, err
}
