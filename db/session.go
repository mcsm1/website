package db

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"slices"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
)

const SESSION_KEY = "Session"
const SESSION_COOKIE_SEP = ":"

type UserSession struct {
	UserId    int
	UUID      uuid.UUID
	Username  string
	Token     string
	expiresAt time.Time
}

func NewUserSession(
	ctx context.Context,
	id int,
	uuid uuid.UUID,
	username string,
) *UserSession {
	buf := make([]byte, 16)
	_, err := rand.Read(buf)
	if err != nil {
		log.Println("Error creating session token:", err)
		return nil
	}

	token := base64.RawURLEncoding.EncodeToString(buf)
	expiresAt := time.Now().Add(time.Hour * 24 * 7)

	sql := `INSERT INTO sessions(token, expires_at, user_id) VALUES ($1, $2, $3)`
	_, err = DB.Exec(ctx, sql, token, expiresAt, id)
	if err != nil {
		log.Println("Error inserting session into db:", err)
		return nil
	}

	sql = `UPDATE users SET last_login = NOW() WHERE id = $1`
	_, err = DB.Exec(ctx, sql, id)
	if err != nil {
		log.Println("Error updating last login:", err)
	}
	return &UserSession{id, uuid, username, token, expiresAt}
}

func GetUserSession(ctx context.Context) (session *UserSession) {
	if s := ctx.Value(SESSION_KEY); s != nil {
		return s.(*UserSession)
	}
	return nil
}

func GetUserSessionFromCookie(r *http.Request) (session UserSession) {
	cookie, err := r.Cookie(SESSION_KEY)
	if err != nil {
		return session
	}

	value := strings.Split(cookie.Value, SESSION_COOKIE_SEP)
	if len(value) < 4 {
		log.Println("Bad session cookie value format (bad actor)")
		return session
	}

	userId, err := strconv.ParseInt(value[0], 10, 64)
	if err != nil {
		log.Println("Bad session cookie userId (bad actor):", err)
		return session
	}

	uuid, err := uuid.Parse(value[1])
	if err != nil {
		log.Println("Bad session cookie UUID (bad actor):", err)
		return session
	}

	username, token := value[2], value[3]
	return UserSession{int(userId), uuid, username, token, cookie.Expires}
}

func (u *UserSession) IsValid(ctx context.Context) (ok bool) {
	if u.UserId == 0 {
		return false
	}

	sql := `SELECT true
			FROM users u INNER JOIN sessions s ON u.id = s.user_id
			WHERE s.token = $1 AND u.id = $2 AND s.expires_at > NOW()`
	err := DB.QueryRow(ctx, sql, u.Token, u.UserId).Scan(&ok)
	if err != nil && !errors.Is(err, ErrNoRows) {
		log.Println("Session verification error:", err)
		return false
	}
	return ok
}

func (u *UserSession) IsAdmin() bool {
	admins := []string{}
	for _, name := range strings.Split(os.Getenv("ADMINS"), ",") {
		admins = append(admins, strings.TrimSpace(name))
	}
	return slices.Contains(admins, u.Username)
}

func (u *UserSession) Delete(ctx context.Context) {
	u.expiresAt = time.Time{}
	if u.UserId == 0 {
		return
	}

	sql := `DELETE FROM sessions
			WHERE expires_at < NOW() OR (user_id = $1 AND token = $2)`
	_, err := DB.Exec(ctx, sql, u.UserId, u.Token)
	if err != nil {
		log.Println("Session deletion error:", err)
	}
}

func (u *UserSession) WithContext(ctx context.Context) context.Context {
	return context.WithValue(ctx, SESSION_KEY, u)
}

func (u *UserSession) Cookie() *http.Cookie {
	value := strings.Join([]string{
		fmt.Sprintf("%d", u.UserId), u.UUID.String(), u.Username, u.Token,
	}, SESSION_COOKIE_SEP)

	return &http.Cookie{
		Name:     SESSION_KEY,
		Value:    value,
		Path:     "/",
		Expires:  u.expiresAt,
		Secure:   true,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
}
