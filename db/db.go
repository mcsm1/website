package db

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

var ErrNoRows = pgx.ErrNoRows
var DB *pgxpool.Pool

func MustConnect() {
	log.Println("Connecting to db...")
	db, err := pgxpool.New(context.Background(), os.Getenv("DB_URL"))
	if err != nil {
		log.Fatal("Error connecting to db: ", err)
	}
	DB = db

	fmt.Print("Creating tables...")
	mustCreateUsersTable()
	mustCreateInviteCodesTable()
	mustCreateSessionsTable()
	fmt.Println()
}

func mustCreateUsersTable() {
	sql := `CREATE TABLE IF NOT EXISTS users (
			id SERIAL PRIMARY KEY,
			uuid UUID NOT NULL,
			username VARCHAR(16) UNIQUE NOT NULL,
			hash VARCHAR(70) NOT NULL,
			invited_by INT REFERENCES users(id),
			joined_at TIMESTAMP DEFAULT NOW(),
			last_login TIMESTAMP DEFAULT NOW()
	)`
	mustCreateTable("users", sql)
}

func mustCreateInviteCodesTable() {
	sql := `CREATE TABLE IF NOT EXISTS invite_codes (
			id SERIAL PRIMARY KEY,
			code VARCHAR(44) UNIQUE NOT NULL,
			created_by INT REFERENCES users(id) NOT NULL,
			expires_at TIMESTAMP NOT NULL
	)`
	mustCreateTable("invite_codes", sql)
}

func mustCreateSessionsTable() {
	sql := `CREATE TABLE IF NOT EXISTS sessions (
			id SERIAL PRIMARY KEY,
			token VARCHAR(22) UNIQUE NOT NULL,
			expires_at TIMESTAMP NOT NULL,
			user_id INT REFERENCES users(id)
	)`
	mustCreateTable("sessions", sql)
}

func mustCreateTable(name, sql string) {
	fmt.Print(" ", name)
	_, err := DB.Exec(context.Background(), sql)
	if err != nil {
		fmt.Println()
		log.Fatal("Error creating table: ", err)
	}
}
