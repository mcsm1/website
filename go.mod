module gitlab.com/davidjmacdonald1/mcsm/website

go 1.22.0

require (
	github.com/a-h/templ v0.2.747
	github.com/google/uuid v1.6.0
	github.com/jackc/pgx/v5 v5.6.0
	github.com/joho/godotenv v1.5.1
	gitlab.com/davidjmacdonald1/mcsm/api v1.0.0
	golang.org/x/crypto v0.17.0
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/jltobler/go-rcon v0.3.0 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
