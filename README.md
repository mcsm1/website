# MCSM Website
This software is a component of the
[Minecraft Server Manager](https://gitlab.com/mcsm1) pack.
See that page for more details.

Its purpose is to create a web ui for users to automatically start the
Mincraft server and to check its state.

# Usage
You will have to fork this repo, and deploy it.
You will also have to setup an sqlite db.

I personally used Railway for this.
There is a `nixpacks.toml` file for easy depoyment.
If you'd like to support me, you may sign up using
[my referral link](https://railway.app?referralCode=CKi-Bf).
I was able to create a volume for my db, and used the free trial.
It seems to cost roughly a cent or two a day and the free trail is $5.

I really don't have much more advice for this, so good luck.

# Env
The website uses environment vars to store secure information,
and for customization.
If there is a .env file, the website will use that first.
If not, it will use Go's normal env vars.

Some of these will not make sense without understanding the design.
Once again, see the top of this file for a link to the main page.

Here are the vars:

- `ADMINS`:
A comma separated list of usernames which should be considered admins.

- `BOOTSTRAP_INVITE_CODE`:
An invite code used only to allow the first user to make an account.
From there, everyone must be invited by another user.

- `DB_PATH`:
The path to the sqlite db.
Since we use sqlite, this may simply be a file.

- `SESSION_KEY`:
A secure password for encrypting user sessions.

- `HOST_IP`:
The public ip address for the server and the host computer running it.

- `HOST_SSH_PORT`:
The port used by the host for ssh.

- `HOST_SSH_USER`:
The user to login as when connecting via ssh.

- `HOST_SSH_KEY`:
The entire host key (ed25519) for the host computer.
You can find this by running `ssh-keyscan -t ed25519 hostname`,

- `HOST_CLI_PATH`:
The file path to the folder containing the CLI app.

- `HOST_CLI_APP`:
The command to run the CLI app from inside of `HOST_CLI_PATH`.

- `SSH_PASS`:
The password set for the ssh key.

- `SSH_KEY`:
The entire private ssh key for ssh connections.

- `WOL_SSH_PORT`:
The port relative to `HOST_IP` used by the WOL device for ssh.

- `WOL_SSH_USER`:
The user to login as when connecting via ssh.

- `WOL_SSH_KEY`:
The entire host key (ed25519) for the WOL computer.
You can find this by running `ssh-keyscan -t ed25519 hostname`,

- `WOL_COMMAND`:
The command to issue to the WOL device via ssh to wake the host.
