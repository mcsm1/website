$(document).ready(function() {
	let $button = $("#invite-code-button");
	let $codeArea = $("#hx-invite-code");
	let $popup = $("#invite-code-popup");

	$codeArea.popup({
		popup: $popup,
		on: "manual",
		position: "bottom center",
		onHide: function(elem) {
			if ($(elem).hasClass("clicked")) return false;
		},
	});

	$codeArea.on("mouseenter", function() {
		if ($(this).val().trim() === "") return;
		$(this).popup("show");
	});

	$codeArea.on("mouseleave", function() {
		$(this).popup("hide");
	});

	$codeArea.on("click", function() {
		if ($(this).val().trim() === "") return;
		$(this).addClass("clicked");

		navigator.clipboard.writeText($codeArea[0].value);
		$popup.text("Copied!");
	});

	$button.on("click", function() {
		$popup.text("Click to copy");
	});

	$(document).on("click", function(event) {
		let length = $codeArea.has(event.target).length
		if (!$codeArea.is(event.target) && length === 0) {
			$codeArea.removeClass("clicked");
			$codeArea.popup("hide");
		}
	});
});
