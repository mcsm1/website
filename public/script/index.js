function buttonBefore(button) {
	button.disabled = true;
	button.classList.add("loading");
}

function buttonAfter(button) {
	button.disabled = false;
	button.classList.remove("loading");
}

function startButtonBefore(button) {
	button.disabled = true;
	button.classList.remove("yellow");
	button.classList.add("disabled");

	let icon = button.querySelector("i");
	icon.classList.remove("power", "off");
	icon.classList.add("spinner", "loading");

	document.getElementById("hx-state").removeAttribute("hx-trigger");
}

function formBefore(form) {
	let button = form.querySelector("button.submit");
	buttonBefore(button);
}

function formAfter(form) {
	let button = form.querySelector("button.submit");
	buttonAfter(button);
}
