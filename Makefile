.PHONY: air run build

air: build
	@go build -o ./tmp/main main.go

build:
	@templ generate

clean:
	 @find . -name "*_templ.go" -type f -delete
