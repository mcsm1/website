package handler

import (
	"net/http"

	"gitlab.com/davidjmacdonald1/mcsm/website/db"
)

func on(next http.HandlerFunc) http.Handler {
	return http.HandlerFunc(next)
}

func isAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session := db.GetUserSessionFromCookie(r)
		if !session.IsValid(r.Context()) {
			http.SetCookie(w, session.Cookie())
			redirect(w, r, "/auth")
			return
		}

		ctx := session.WithContext(r.Context())
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func isAdmin(next http.Handler) http.Handler {
	return isAuth(on(func(w http.ResponseWriter, r *http.Request) {
		session := db.GetUserSession(r.Context())
		if !session.IsAdmin() {
			redirect(w, r, "/")
		} else {
			next.ServeHTTP(w, r)
		}
	}))
}
