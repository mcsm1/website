package handler

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"

	"github.com/google/uuid"
	"gitlab.com/davidjmacdonald1/mcsm/website/db"
	"gitlab.com/davidjmacdonald1/mcsm/website/view"
	"golang.org/x/crypto/bcrypt"
)

func handleAuth(r Router) {
	r.HandleFunc("GET /{$}", showAuthPage)
	r.HandleFunc("POST /query", queryUser)
	r.HandleFunc("POST /login", loginUser)
	r.HandleFunc("POST /signup", signupUser)
	r.HandleFunc("POST /logout", logoutUser)
}

func showAuthPage(w http.ResponseWriter, r *http.Request) {
	session := db.GetUserSessionFromCookie(r)
	if session.IsValid(r.Context()) {
		redirect(w, r, "/")
		return
	}

	if session.Username != "" {
		render(w, r, view.AuthPage(view.AuthLoginForm(session.Username, "")))
	} else {
		render(w, r, view.AuthPage(view.AuthQueryForm("", "")))
	}
}

func queryUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		render(w, r, view.AuthQueryForm("", "Bad form request"))
		return
	}
	username := r.FormValue("username")

	err = verifyUsernameForm(username)
	if err != nil {
		render(w, r, view.AuthQueryForm(username, err.Error()))
		return
	}

	var exists bool
	sql := `SELECT true FROM users WHERE username = $1`
	err = db.DB.QueryRow(r.Context(), sql, username).Scan(&exists)
	if err != nil && !errors.Is(err, db.ErrNoRows) {
		log.Println("Query user error:", err)
		render(w, r, view.AuthQueryForm(username, ISE))
		return
	}

	if exists {
		render(w, r, view.AuthLoginForm(username, ""))
	} else {
		render(w, r, view.AuthSignupForm(username, ""))
	}
}

func loginUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		render(w, r, view.AuthLoginForm("", "Bad form request"))
		return
	}
	username, password := r.FormValue("username"), r.FormValue("password")

	err = verifyLoginForm(username, password)
	if err != nil {
		render(w, r, view.AuthLoginForm(username, err.Error()))
		return
	}

	var (
		id   int
		uuid uuid.UUID
		hash []byte
	)
	sql := `SELECT id, uuid, hash FROM users WHERE username = $1`
	err = db.DB.QueryRow(r.Context(), sql, username).Scan(&id, &uuid, &hash)
	if errors.Is(err, db.ErrNoRows) {
		render(w, r, view.AuthSignupForm(username, "User not found"))
		return
	}
	if err != nil {
		log.Println("Login user error:", err)
		render(w, r, view.AuthLoginForm(username, ISE))
		return
	}

	if bcrypt.CompareHashAndPassword(hash, []byte(password)) != nil {
		render(w, r, view.AuthLoginForm(username, "Incorrect password"))
		return
	}

	session := db.NewUserSession(r.Context(), id, uuid, username)
	if session == nil {
		render(w, r, view.AuthLoginForm(username, ISE))
		return
	}

	http.SetCookie(w, session.Cookie())
	redirect(w, r, "/")
}

func signupUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		render(w, r, view.AuthSignupForm("", "Bad form request"))
		return
	}
	username, password := r.FormValue("username"), r.FormValue("password")
	confirmation, code := r.FormValue("confirm-password"), r.FormValue("code")

	err = verifySignupForm(username, password, confirmation, code)
	if err != nil {
		render(w, r, view.AuthSignupForm(username, err.Error()))
		return
	}

	uuid, err := fetchUUID(username)
	if err != nil {
		render(w, r, view.AuthSignupForm(username, err.Error()))
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		log.Println("Error hashing password:", err)
		render(w, r, view.AuthSignupForm(username, ISE))
		return
	}

	invitedBy, err := checkInviteCode(r.Context(), code)
	if err != nil {
		render(w, r, view.AuthSignupForm(username, err.Error()))
		return
	}

	var id int
	sql := `INSERT INTO users(uuid, username, hash, invited_by)
			VALUES ($1, $2, $3, NULLIF($4, -1)) RETURNING id`
	err = db.DB.QueryRow(
		r.Context(), sql, uuid, username, hash, invitedBy,
	).Scan(&id)
	if err != nil {
		log.Println("Error creating user:", err)
		render(w, r, view.AuthSignupForm(username, ISE))
		return
	}

	session := db.NewUserSession(r.Context(), id, uuid, username)
	if session == nil {
		render(w, r, view.AuthLoginForm(username, ISE))
		return
	}

	http.SetCookie(w, session.Cookie())
	redirect(w, r, "/")
}

func logoutUser(w http.ResponseWriter, r *http.Request) {
	session := db.GetUserSessionFromCookie(r)
	session.Delete(r.Context())
	http.SetCookie(w, session.Cookie())
	redirect(w, r, "/")
}

var usernamePattern = regexp.MustCompile(`^\w{3,16}$`)

func verifySignupForm(username, password, confirmation, code string) error {
	err := verifyLoginForm(username, password)
	if err != nil {
		return err
	}

	if password != confirmation {
		return fmt.Errorf("Passwords do not match")
	}
	if code == "" {
		return fmt.Errorf("Invite Code must be provided")
	}

	return nil
}

func verifyLoginForm(username, password string) error {
	err := verifyUsernameForm(username)
	if err != nil {
		return err
	}

	if password == "" {
		return fmt.Errorf("Password must be provided")
	}
	if len(password) < 10 || len(password) > 70 {
		return fmt.Errorf("Password must be between 10 and 70 chars")
	}

	return nil
}

func verifyUsernameForm(username string) error {
	if username == "" {
		return fmt.Errorf("Username must be provided")
	}
	if !usernamePattern.MatchString(username) {
		return fmt.Errorf("Username is not valid")
	}
	return nil
}

func fetchUUID(username string) (id uuid.UUID, err error) {
	msg := errors.New("Cannot verify username")
	const url = "https://api.mojang.com/users/profiles/minecraft/"
	res, err := http.Get(url + username)
	if err != nil || res.StatusCode != 200 {
		if err == nil {
			res.Body.Close()
		}
		log.Println("Bad uuid response:", err)
		return id, msg
	}
	defer res.Body.Close()

	uuidRes := new(struct {
		UUID string `json:"id"`
	})
	err = json.NewDecoder(res.Body).Decode(uuidRes)
	if err != nil {
		log.Println("Bad uuid response decode:", err)
		return id, msg
	}

	id, err = uuid.Parse(uuidRes.UUID)
	if err != nil {
		log.Println("Bad uuid response parse:", err)
		return id, msg
	}
	return id, nil
}

func checkInviteCode(ctx context.Context, code string) (by int, err error) {
	sql := `SELECT created_by FROM invite_codes
			WHERE code = $1 AND expires_at > NOW()`
	err = db.DB.QueryRow(ctx, sql, code).Scan(&by)
	if errors.Is(err, db.ErrNoRows) {
		return tryBootstrapInviteCode(ctx, code)
	}
	if err != nil {
		log.Println("Error checking invite code:", err)
		return by, errors.New(ISE)
	}

	sql = `DELETE FROM invite_codes
		   WHERE expires_at < NOW() OR code = $1`
	_, err = db.DB.Exec(ctx, sql, code)
	if err != nil {
		log.Println("Error deleting invite codes:", err)
	}
	return by, nil
}

func tryBootstrapInviteCode(ctx context.Context, code string) (int, error) {
	var count int
	sql := `SELECT count(*) FROM (SELECT 1 FROM users LIMIT 1)`
	err := db.DB.QueryRow(ctx, sql).Scan(&count)
	if err != nil {
		log.Println("Error checking user count for bootstrapping:", err)
		return 0, errors.New(ISE)
	}

	c := os.Getenv("BOOTSTRAP_INVITE_CODE")
	if count == 0 && c != "" && c == code {
		log.Println("WARNING! Bootstrap invite code used!")
		return -1, nil
	}
	return 0, fmt.Errorf("Invite code is not valid")
}
