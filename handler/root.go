package handler

import (
	"crypto/rand"
	"encoding/base64"
	"log"
	"net/http"
	"time"

	"gitlab.com/davidjmacdonald1/mcsm/website/cmd"
	"gitlab.com/davidjmacdonald1/mcsm/website/db"
	"gitlab.com/davidjmacdonald1/mcsm/website/view"
	"gitlab.com/davidjmacdonald1/mcsm/website/view/tag"
)

func handleRoot(r Router) {
	r.Handle("GET /{$}", isAuth(on(getHomePage)))
	r.Handle("GET /state", isAuth(on(getState)))
	r.Handle("POST /start-server", isAuth(on(startServer)))
	r.Handle("POST /invite-code", isAuth(on(generateInviteCode)))
}

func getHomePage(w http.ResponseWriter, r *http.Request) {
	session := db.GetUserSession(r.Context())
	render(w, r, view.HomePage(session))
}

func getState(w http.ResponseWriter, r *http.Request) {
	state := cmd.SendHost("getFullState")
	if state.IsOn && state.Server.IsLoaded {
		sql := `SELECT username FROM users`
		rows, err := db.DB.Query(r.Context(), sql)
		if err != nil {
			log.Println("Error querying all usernames:", err)
			goto exit
		}
		defer rows.Close()

		for rows.Next() {
			var username string
			err := rows.Scan(&username)
			if err != nil {
				log.Println("Error scanning username:", err)
				goto exit
			}

			s := cmd.SendHost("whitelistPlayer " + username)
			if s.Error != "" {
				log.Println("Error whitelisting user:", s.Error)
				goto exit
			}
		}
	}

exit:
	render(w, r, tag.State(state))
}

func startServer(w http.ResponseWriter, r *http.Request) {
	if !cmd.WakeHost() {
		state := cmd.MakeErrorState("The computer did not wake up")
		render(w, r, tag.State(state))
		return
	}

	state := cmd.SendHost("startServer")
	if state.Error != "" {
		state.Error = "There was a problem starting the server"
		render(w, r, tag.State(state))
	}

	time.Sleep(3000 * time.Millisecond)
	state = cmd.SendHost("getFullState")
	render(w, r, tag.State(state))
}

func generateInviteCode(w http.ResponseWriter, r *http.Request) {
	buf := make([]byte, 32)
	_, err := rand.Read(buf)
	if err != nil {
		log.Println("Error creating invite code:", err)
		w.Write([]byte(ISE))
		return
	}

	code := base64.URLEncoding.EncodeToString(buf)
	session := db.GetUserSession(r.Context())
	expiresAt := time.Now().Add(48 * time.Hour)

	sql := `INSERT INTO invite_codes(code, created_by, expires_at)
			VALUES ($1, $2, $3)`
	_, err = db.DB.Exec(r.Context(), sql, code, session.UserId, expiresAt)
	if err != nil {
		log.Println("Error storing invite code:", err)
		w.Write([]byte(ISE))
		return
	}
	w.Write([]byte(code))
}
