package handler

import (
	"fmt"
	"net/http"
	"strings"
)

type Router struct {
	*http.ServeMux
}

func MakeRouter() Router {
	r := Router{http.NewServeMux()}
	r.Handle("/", http.FileServer(http.Dir("./public")))

	handleRoot(r)
	handleAuth(r.makeSubRouter("/auth/"))
	handleUser(r.makeSubRouter("/user/"))
	handleAdmin(r.makeSubRouter("/admin/"))
	return r
}

func (r Router) makeSubRouter(path string) Router {
	if !strings.HasPrefix(path, "/") || !strings.HasSuffix(path, "/") {
		panic(fmt.Errorf(`paths must have the form "/path/"; got %q`, path))
	}

	subRouter := Router{http.NewServeMux()}
	prefix := strings.TrimRight(path, "/")
	r.Handle(path, http.StripPrefix(prefix, subRouter))
	return subRouter
}
