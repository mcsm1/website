package handler

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/davidjmacdonald1/mcsm/website/cmd"
	"gitlab.com/davidjmacdonald1/mcsm/website/db"
	"gitlab.com/davidjmacdonald1/mcsm/website/view"
	"gitlab.com/davidjmacdonald1/mcsm/website/view/tag"
)

func handleAdmin(r Router) {
	r.Handle("GET /{$}", isAdmin(on(showAdminPage)))

	r.Handle("GET /system-dashboard", isAdmin(on(getAdminSystemDashboard)))
	r.Handle("POST /wake", isAdmin(on(wakeAsAdmin)))
	r.Handle("POST /shutdown", isAdmin(on(shutdownAsAdmin)))
	r.Handle("POST /lock", isAdmin(on(lockAsAdmin)))
	r.Handle("POST /unlock", isAdmin(on(unlockAsAdmin)))
	r.Handle("POST /start-server", isAdmin(on(startServerAsAdmin)))
	r.Handle("POST /stop-server", isAdmin(on(stopServerAsAdmin)))
	r.Handle("POST /update-whitelist", isAdmin(on(updateWhitelistAsAdmin)))

	r.Handle("GET /database-dashboard", isAdmin(on(getAdminDatabaseDashboard)))
	r.Handle("POST /clear-expired-data", isAdmin(on(clearExpiredDataAsAdmin)))
}

func showAdminPage(w http.ResponseWriter, r *http.Request) {
	session := db.GetUserSession(r.Context())
	render(w, r, view.AdminPage(session))
}

func getAdminSystemDashboard(w http.ResponseWriter, r *http.Request) {
	state := cmd.SendHost("getFullState")
	render(w, r, tag.AdminSystemDashboard(state))
}

func wakeAsAdmin(w http.ResponseWriter, r *http.Request) {
	if !cmd.WakeHost() {
		state := cmd.MakeErrorState("The computer did not wake up")
		render(w, r, tag.AdminSystemDashboard(state))
		return
	}

	time.Sleep(1 * time.Second)
	state := cmd.SendHost("getFullState")
	render(w, r, tag.AdminSystemDashboard(state))
}

func shutdownAsAdmin(w http.ResponseWriter, r *http.Request) {
	state := cmd.SendHost("shutdown")
	if state.Error != "" {
		render(w, r, tag.AdminSystemDashboard(state))
		return
	}
	render(w, r, tag.AdminSystemDashboard(cmd.ZeroState))
}

func lockAsAdmin(w http.ResponseWriter, r *http.Request) {
	state := cmd.SendHost("lock")
	if state.Error != "" {
		render(w, r, tag.AdminSystemDashboard(state))
		return
	}

	state = cmd.SendHost("getFullState")
	render(w, r, tag.AdminSystemDashboard(state))
}

func unlockAsAdmin(w http.ResponseWriter, r *http.Request) {
	state := cmd.SendHost("unlock")
	if state.Error != "" {
		render(w, r, tag.AdminSystemDashboard(state))
		return
	}

	state = cmd.SendHost("getFullState")
	render(w, r, tag.AdminSystemDashboard(state))
}

func startServerAsAdmin(w http.ResponseWriter, r *http.Request) {
	state := cmd.SendHost("startServer")
	if state.Error != "" {
		render(w, r, tag.AdminSystemDashboard(state))
		return
	}

	time.Sleep(3 * time.Second)
	state = cmd.SendHost("getFullState")
	render(w, r, tag.AdminSystemDashboard(state))
}

func stopServerAsAdmin(w http.ResponseWriter, r *http.Request) {
	state := cmd.SendHost("stopServer")
	if state.Error != "" {
		render(w, r, tag.AdminSystemDashboard(state))
		return
	}

	time.Sleep(3 * time.Second)
	state = cmd.SendHost("getFullState")
	render(w, r, tag.AdminSystemDashboard(state))
}

func updateWhitelistAsAdmin(w http.ResponseWriter, r *http.Request) {
	sql := `SELECT username FROM users`
	rows, err := db.DB.Query(r.Context(), sql)
	if err != nil {
		log.Println("Error querying all usernames:", err)
		goto exit
	}
	defer rows.Close()

	for rows.Next() {
		var username string
		err := rows.Scan(&username)
		if err != nil {
			log.Println("Error scanning username:", err)
			goto exit
		}

		state := cmd.SendHost("whitelistPlayer " + username)
		if state.Error != "" {
			log.Println("Error whitelisting user:", state.Error)
			goto exit
		}
	}

exit:
	state := cmd.SendHost("getFullState")
	render(w, r, tag.AdminSystemDashboard(state))
}

func getAdminDatabaseDashboard(w http.ResponseWriter, r *http.Request) {
	render(w, r, tag.AdminDatabaseDashboard())
}

func clearExpiredDataAsAdmin(w http.ResponseWriter, r *http.Request) {
	sql := `DELETE FROM invite_codes WHERE expires_at < NOW()`
	_, err := db.DB.Exec(r.Context(), sql)
	if err != nil {
		log.Println("Error deleting old invite codes:", err)
	}

	sql = `DELETE FROM sessions WHERE expires_at < NOW()`
	_, err = db.DB.Exec(r.Context(), sql)
	if err != nil {
		log.Println("Error deleting old sessions:", err)
	}

	render(w, r, tag.AdminDatabaseDashboard())
}
