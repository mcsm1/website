package handler

import (
	"log"
	"net/http"

	"github.com/a-h/templ"
)

const ISE = "Internal server error"

func render(w http.ResponseWriter, r *http.Request, t templ.Component) {
	err := t.Render(r.Context(), w)
	if err != nil {
		log.Println("Rendering err:", err)
	}
}

func renderHandler(t templ.Component) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		render(w, r, t)
	})
}

func redirect(w http.ResponseWriter, r *http.Request, url string) {
	if r.Header.Get("Hx-Request") == "true" {
		w.Header().Set("Hx-Redirect", url)
		w.WriteHeader(http.StatusSeeOther)
	} else {
		http.Redirect(w, r, url, http.StatusSeeOther)
	}
}
