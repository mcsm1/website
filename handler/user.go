package handler

import (
	"net/http"

	"gitlab.com/davidjmacdonald1/mcsm/website/db"
	"gitlab.com/davidjmacdonald1/mcsm/website/view"
)

func handleUser(r Router) {
	r.Handle("GET /{id}", isAuth(on(showAccountPage)))
}

func showAccountPage(w http.ResponseWriter, r *http.Request) {
	id := r.PathValue("id")
	session := db.GetUserSession(r.Context())
	if session.UUID.String() != id {
		redirect(w, r, "/")
		return
	}
	render(w, r, view.UserPage(session))
}
